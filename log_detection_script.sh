#!/bin/bash

#Define the regex pattern
regex_pattern="\b(?:Log\.|Timber\.|\.Log|\.Timer)\b"

# Search for log statements in Java and Kotlin files
LOGS_FOUND=$(grep -r -E "$regex_pattern" ./app/src/)

if [ -n "$LOGS_FOUND" ]; then
  echo "Log statements found in the code:"
  echo "$LOGS_FOUND"
  exit 1
else
  echo "No log statements found."
  exit 0
fi